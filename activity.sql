-- 1
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5
SELECT customerName FROM customers WHERE state IS NULL;

-- 6
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10
SELECT DISTINCT country FROM customers;

-- 11
SELECT DISTINCT status FROM orders;

-- 12
SELECT customerName, country FROM customers WHERE country IN("USA", "France", "Canada");

-- 13
SELECT firstName, lastName, city FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
	WHERE offices.officeCode = 5;

-- 14
SELECT customerName FROM employees
	JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE firstName = "Leslie" AND lastName="Thompson";

-- 15
SELECT productName, customerName FROM customers 
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports";

-- 16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	WHERE customers.country = offices.country;

-- 17
SELECT products.productName, products.quantityInStock FROM productlines
	JOIN products ON productlines.productLine = products.productLine
	WHERE products.productLine = "Planes" AND products.quantityInStock < 1000;

-- 18
SELECT customerName FROM customers WHERE phone LIKE "+81%";